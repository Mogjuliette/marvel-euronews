<?php

namespace App\Tests;

use App\Entity\Comic;
use PHPUnit\Framework\TestCase;

class ComicTest extends TestCase
{
    /**Un test unitaire très simple qui vérifie la bonne marche du constructeur de la classe Comic*/
    public function testClassConstructor(): void
    {
        $this->assertTrue(true);
        $comic = new Comic('Avengers', '2023-03-13','url');
        $comic->setId(1);
        $this->assertIsString($comic->getTitle()); 
        $this->assertIsString($comic->getDate());
        $this->assertIsString($comic->getImage());
        $this->assertIsInt($comic->getId());
    }
}
