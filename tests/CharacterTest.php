<?php

namespace App\Tests;

use App\Entity\Character;
use App\Entity\Comic;
use PHPUnit\Framework\TestCase;

class CharacterTest extends TestCase
{
    /**Un test unitaire très simple qui vérifie la bonne marche du constructeur de la classe Character*/
    public function testSomething(): void
    {
        $this->assertTrue(true);
        $comic = new Comic('Avengers', '2023-03-13','url');
        $comic->setId(1);
        $character = new Character('Nom du perso','description du perso','url image', $comic);
        $character->setId(1);
        $this->assertIsString($character->getName());
        $this->assertIsString($character->getDescription());
        $this->assertIsString($character->getImage());
        $this->assertIsInt($character->getId());
        $this->assertIsString($character->getComic()->getTitle());
        $this->assertIsString($character->getComic()->getDate());
        $this->assertIsString($character->getComic()->getImage());
        $this->assertIsInt($character->getComic()->getId());

    }
}
