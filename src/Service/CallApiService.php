<?php

namespace App\Service;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CallApiService
{   
    /*Dans ce fichier, j'ai externalisé les appels à l'API, avec trois fonctions différentes pour les trois requêtes que j'utilise. */
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }
    public function getComicData(int $page): array
    {
        $offset = $page*50 - 50;
        $response = $this->client->request(
            'GET',
            'http://gateway.marvel.com/v1/public/comics?ts=1&apikey=8cd272b6f6881d5c362dc9ae8cde8344&hash=52373a5bcd7dabcca80ce7fc16cacb9f&titleStartsWith=Avengers&orderBy=onsaleDate&limit=50&offset='.$offset
            /*Avec cette url, on passe en argument l'order by 'on sale date', le fait que le titre commence par Avengers et la limite de 50 résultat par page. L'argument offset permet de paramétrer la requête pour une page différente. */
        );
        $dataList = $response->toArray();

        $results = $dataList['data'];

        return $results;
    }

    public function getCharacterData(int $id): array
    {
        
        $response = $this->client->request(
            'GET',
            'http://gateway.marvel.com/v1/public/comics/'.$id.'/characters?ts=1&apikey=8cd272b6f6881d5c362dc9ae8cde8344&hash=52373a5bcd7dabcca80ce7fc16cacb9f'
        );
        $dataList = $response->toArray();

        $results = $dataList['data'];

        return $results;
    }

    public function getOneComicData(int $id): array
    {
        $response = $this->client->request(
            'GET',
            'https://gateway.marvel.com:443/v1/public/comics/'.$id.'?ts=1&apikey=8cd272b6f6881d5c362dc9ae8cde8344&hash=52373a5bcd7dabcca80ce7fc16cacb9f'
        );
        $dataList = $response->toArray();

        $results = $dataList['data'];

        return $results;
    }
}


