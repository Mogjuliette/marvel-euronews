<?php

namespace App\Controller;

use App\Entity\Character;
use App\Entity\Comic;
use App\Service\CallApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ComicController extends AbstractController
{
/*  Cette route est la route de base car elle affiche tout les comics Avengers à la page 1. Elle pourrait être remplacée par la route suivante. Je l'ai principalement gardée pour respecter la consigne en termes d'url*/ 
    #[Route('/comics/avengers', name: 'app_comics_one')]
    public function index(CallApiService $callApiService): Response
    {
        /*Dans un premier temps, mon code était plus simple et se contentait de retourner le résultat de la requête API. Finalement je l'ai rendu plus complexe pour pouvoir instancier les résultats de la requête avec la classe Comic.*/
        $resultats = $callApiService->getComicData(1)['results'];
        $array = [];
        foreach ($resultats as $resultat) {
            /*On vérifie qu'il y a bien une image */
            $src = "";
            if($resultat['images']){
                $src = ($resultat['images'][0]['path'].'.'.$resultat['images'][0]['extension']);
            }
            $comic = new Comic($resultat['title'],$resultat['dates'][0]['date'],$src);
            $comic->setId($resultat['id']);
            $array[] = $comic;
        } 

        /*On retourne un tableau de comics de classe Comics. La requête data sert juste à calculer le nombre total de pages. */
        return $this->render('comic/index.html.twig', [
            'data' => $callApiService->getComicData(1),
            'page' => 1,
            'array' => $array,
        ]);
    }

    /*Cette route est globalement la même que la précédente mais elle permet d'afficher les différentes pages de résultat*/
    #[Route('/comics/avengers/{page}', name: 'app_comics')]
    public function indexByPage(CallApiService $callApiService, int $page): Response
    {   
        $resultats = $callApiService->getComicData($page)['results'];
        $array = [];
        foreach ($resultats as $resultat) {
            /*On vérifie qu'il y a bien une image */
            $src = "";
            if($resultat['images']){
                $src = ($resultat['images'][0]['path'].'.'.$resultat['images'][0]['extension']);
            }
            $comic = new Comic($resultat['title'],$resultat['dates'][0]['date'],$src);
            $comic->setId($resultat['id']);
            $array[] = $comic;
        } 

        return $this->render('comic/index.html.twig', [
            'data' => $callApiService->getComicData($page),
            'page' => $page,
            'array' => $array,

        ]);
    }

    /*Cette route permet d'afficher tous les personnages d'un comic */
    #[Route('/comics/avengers/{id}/characters', name: 'app_characters')]
    public function character(CallApiService $callApiService, int $id): Response
    {   
        /*De même, j'ai choisi de transformer le résultat de la requête en personnages de classe Character. Comme chaque personnage est lié à un comic, j'ai créé une classe Character qui contient une propriété de classe Comic, pour avoir de la dépendance entre classes (et préfigurer une relation one to many) */
        $resultat = $callApiService->getOneComicData($id)['results'][0];
        $comic = new Comic($resultat['title'],$resultat['dates'][0]['date'],($resultat['images'][0]['path'].'.'.$resultat['images'][0]['extension']));
        $charResults = $callApiService->getCharacterData($id)['results'];
        $array = [];
        foreach ($charResults as $charResult) {
            /**On vérifie qu'il y a bien une image */
            $src = "";
            if($charResult['thumbnail']){
                $src = ($charResult['thumbnail']['path'].'.'.$charResult['thumbnail']['extension']);
            }
            $character = new Character($charResult['name'],$charResult['description'],$src,$comic);
            $character->setComic($comic);
            $array[] = $character;
        }
        
        return $this->render('comic/character.html.twig', [
            'id' => $id,
            'array' => $array,
        ]);
    }
}
