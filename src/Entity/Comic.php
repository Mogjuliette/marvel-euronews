<?php

namespace App\Entity;

class Comic{
	private ?int $id;

    private string $title;

	private string $date;

	private ?string $image;

	/**
	 * @return string
	 */
	public function getTitle(): string {
		return $this->title;
	}
	
	/**
	 * @param string $title 
	 * @return self
	 */
	public function setTitle(string $title): self {
		$this->title = $title;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDate(): string {
		return $this->date;
	}
	
	/**
	 * @param string $date 
	 * @return self
	 */
	public function setDate(string $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getImage(): ?string {
		return $this->image;
	}
	
	/**
	 * @param string|null $image 
	 * @return self
	 */
	public function setImage(?string $image): self {
		$this->image = $image;
		return $this;
	}

	/**
	 * @param string $title
	 * @param string $date
	 * @param string|null $image
	 */
	public function __construct(string $title, string $date, ?string $image) {
		$this->title = $title;
		$this->date = $date;
		$this->image = $image;
	}

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
}