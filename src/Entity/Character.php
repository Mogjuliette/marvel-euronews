<?php

namespace App\Entity;
use App\Entity\Comic;

class Character{

    private ?int $id;

    private string $name;

    private string $description;

    private string $image;

    private Comic $comic;


    /**
     * @param int|null $id
     * @param string $name
     * @param string $description
     * @param string $image
     * @param Comic $comic
     */
    public function __construct(string $name, string $description, string $image, Comic $comic) {
    	$this->name = $name;
    	$this->description = $description;
    	$this->image = $image;
    	$this->comic = $comic;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
	
	/**
	 * @param string $name 
	 * @return self
	 */
	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getDescription(): string {
		return $this->description;
	}
	
	/**
	 * @param string $description 
	 * @return self
	 */
	public function setDescription(string $description): self {
		$this->description = $description;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getImage(): string {
		return $this->image;
	}
	
	/**
	 * @param string $image 
	 * @return self
	 */
	public function setImage(string $image): self {
		$this->image = $image;
		return $this;
	}
	
	/**
	 * @return Comic
	 */
	public function getComic(): Comic {
		return $this->comic;
	}
	
	/**
	 * @param Comic $comic 
	 * @return self
	 */
	public function setComic(Comic $comic): self {
		$this->comic = $comic;
		return $this;
	}
}