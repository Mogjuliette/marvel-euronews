<?php
use App\Controller\TestController;

require '../vendor/autoload.php';

$loader = new \Twig\Loader\ArrayLoader([
    'index' => 'Hello {{ name }}!',
]);
$twig = new \Twig\Environment($loader);

echo $twig->render('index', ['name' => 'Juliette']);

/* $test = new TestController;
$test->index();  */

/* $list = ['noir','bleu','rouge','vert','blanc']; */
?>
